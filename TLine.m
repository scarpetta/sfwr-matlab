classdef TLine < handle
    properties
        name = 'TLine'
        dx = 0 % cell length
        Zsource = @(w) 50 + 1i*w*0 % generatore impedance
        Zmeas = @(w) 1e9 ./ (1 + 1i*w*0e-12*1e6) % voltmeter impedance
        Zload = @(w) 1e9 ./ (1 + 1i*w*0e-12*1e9) % load impedance
        lines = {} % transmission line models
        L_line
        C_line
        R_line
        G_line
        Nxi % number of cells per earch line
        lumped_series_fault_pos = [] % relative position of series faults
        lumped_series_fault_Z % impedance of series faults
        lumped_parallel_fault_pos = [] % relative position of parallel faults
        lumped_parallel_fault_Z % impedance of parallel faults
    end
    methods
        function obj = TLine()
            addpath('./linelab-simulator/lib')
        end
        
        function add_line(obj, line_model)
            obj.lines{end + 1} = line_model;
        end
        
        function v_line = max_line_velocity(obj)
            [L0_line, ~, C0_line, ~, ~] = TLprimaryParameters(obj.lines);
            v_lines = sqrt(1 ./ (L0_line .* C0_line));
            v_lines(v_lines == inf) = 0;
            v_line = max(v_lines);
        end
        
        function [l_line, l] = line_length(obj)
            [~, ~, ~, ~, l] = TLprimaryParameters(obj.lines);
            l_line = sum(l);
        end
        
        function update_primary_parameters(obj)
            if obj.dx == 0
                error('Error: set dx before');
            end
            
            [L0_line , R0_line, C0_line, G0_line, l] = TLprimaryParameters(obj.lines);
            l_line = sum(l);
            
            Nx = ceil(l_line / obj.dx);
            
            % Computing parameter profiles
            obj.Nxi = round(Nx * l / sum(l));
            % FIXME find a cleaner solution to ensure sum(Nxi) = Nx
            if sum(obj.Nxi) - Nx ~= 0
                obj.Nxi(1) = obj.Nxi(1) - (sum(obj.Nxi) - Nx);
            end
            
            obj.L_line = zeros(1,Nx);
            obj.C_line = zeros(1,Nx);
            obj.R_line = zeros(1,Nx);
            obj.G_line = zeros(1,Nx);
            for i = 1:length(obj.Nxi)
                if i == 1
                    istart = 1;
                else
                    istart = obj.Nxi(i-1)+1;
                end
                
                obj.L_line(istart:istart+obj.Nxi(i)-1) = L0_line(i) + TLcomposeProfile(obj.Nxi(i), obj.lines{i}.Lprofile);
                obj.R_line(istart:istart+obj.Nxi(i)-1) = R0_line(i) + TLcomposeProfile(obj.Nxi(i), obj.lines{i}.Rprofile);
                obj.C_line(istart:istart+obj.Nxi(i)-1) = C0_line(i) + TLcomposeProfile(obj.Nxi(i), obj.lines{i}.Cprofile);
                obj.G_line(istart:istart+obj.Nxi(i)-1) = G0_line(i) + TLcomposeProfile(obj.Nxi(i), obj.lines{i}.Gprofile);
            end
        end
        
        function VrefH = simulate(obj, omega, update)
            filename = sprintf('cache/%s.mat', obj.name);
            
            if nargin == 2 || (nargin == 3 && update == false)
                if exist(filename, 'file') == 2
                    var = load(filename);
                    l_omega = var.omega;
                    l_dx = var.l_dx;
                    VrefH = var.VrefH;
                    
                    if length(l_omega) == length(omega) && sum(l_omega ~= omega) == 0 && l_dx == obj.dx
                        return
                    end
                end
            end
            
            obj.update_primary_parameters();
            
            measurement_node = 1;
            
            % Frequency domain simulations
            VrefH = zeros(size(omega));
            
            tic
            
            if isempty(gcp('nocreate'))
                disp('LineLab >> Performing frequency domain simulations (single thread)')
                for kw = 1:length(omega)
                    % Source, measurement and load impedances
                    YS_w = 1/obj.Zsource(omega(kw));
                    Ymeas_w = 1/obj.Zmeas(omega(kw));
                    Yload_w = 1/obj.Zload(omega(kw));
                    % Series dispersion
                    YL = TLskinEffect(obj.Nxi, obj.lines, omega(kw), obj.R_line, obj.L_line) / obj.dx;
                    % Parallel dispersion
                    YC = TLepsilonDispersion(obj.Nxi, obj.lines, omega(kw), obj.G_line, obj.C_line) * obj.dx;
                    
                    % Lumped faults
                    for i = 1:length(obj.lumped_series_fault_pos)
                        node = round(obj.lumped_series_fault_pos(i)*length(YL));
                        YL(node) = 1/(1/YL(node) + obj.lumped_series_fault_Z{i}(omega(kw)));
                    end
                    for i = 1:length(obj.lumped_parallel_fault_pos)
                        node = round(obj.lumped_parallel_fault_pos(i)*length(YC));
                        YC(node) = YC(node) + 1/obj.lumped_parallel_fault_Z{i}(omega(kw));
                    end
                    
                    VrefH(kw) = TLsolveCircuit(YS_w, Ymeas_w, YL, YC, Yload_w, measurement_node);
                end
            else
                disp('LineLab >> Performing frequency domain simulations (multi thread)')
                parfor kw = 1:length(omega)
                    % Source, measurement and load impedances
                    YS_w = 1/obj.Zsource(omega(kw));
                    Ymeas_w = 1/obj.Zmeas(omega(kw));
                    Yload_w = 1/obj.Zload(omega(kw));
                    % Series dispersion
                    YL = TLskinEffect(obj.Nxi, obj.lines, omega(kw), obj.R_line, obj.L_line) / obj.dx;
                    % Parallel dispersion
                    YC = TLepsilonDispersion(obj.Nxi, obj.lines, omega(kw), obj.G_line, obj.C_line) * obj.dx;
                    
                    % Lumped faults
                    for i = 1:length(obj.lumped_series_fault_pos)
                        node = round(obj.lumped_series_fault_pos(i)*length(YL));
                        YL(node) = 1/(1/YL(node) + obj.lumped_series_fault_Z{i}(omega(kw)));
                    end
                    for i = 1:length(obj.lumped_parallel_fault_pos)
                        node = round(obj.lumped_parallel_fault_pos(i)*length(YC));
                        YC(node) = YC(node) + 1/obj.lumped_parallel_fault_Z{i}(omega(kw));
                    end
                    
                    VrefH(kw) = TLsolveCircuit(YS_w, Ymeas_w, YL, YC, Yload_w, measurement_node);
                end
            end
            
            fprintf('LineLab >> Simulation completed in %f s\n', toc)
            
            [~, ~, ~] = mkdir('cache');
            l_dx = obj.dx;
            save(filename, 'omega', 'VrefH', 'l_dx');
        end
        
        function VrefH0 = dc_response(obj)
            measurement_node = 1;
            % DC response
            % FIXME causes a lot of trouble: deprecate?
            if sum(obj.R_line) ~= 0 || sum(obj.G_line) ~= 0
                VrefH0 = TLsolveCircuit(1/obj.Zsource(0), 1/obj.Zmeas(0), 1./obj.R_line, obj.G_line, 1/obj.Zload(0), measurement_node);
            else
                if isinf(obj.Zload(0))
                    VrefH0 = 1;
                else
                    VrefH0 = obj.Zload(0) / (obj.Zload(0) + obj.Zsource(0));
                end
            end
        end
        
        function tdr = get_tdr(obj, t, Vs, update)
            if nargin == 3
                update = false;
            end
            
            Nt = length(t);
            dt = min(diff(t));
            fs = 1 / dt;
            f_max = fs / 2;
            Nh = floor(Nt / 2);
            df = fs / Nt;
            
            lambda_min = obj.max_line_velocity() / f_max;
            obj.dx = lambda_min / 20;
            Nx = ceil(obj.line_length()/obj.dx);
            obj.dx = obj.line_length()/Nx;
            
            omega = 2 * pi * df * (1:Nh)';
            
            Vs_fft = fft(Vs) / Nt;
            VrefH = obj.simulate(omega, update);
            
            H = zeros(size(Vs));
            H(2 : Nh + 1) = VrefH;
            H(end - Nh + 1 : end) = conj(flipud(VrefH));
            
            tdr_fft = H .* Vs_fft;
            tdr = ifft(tdr_fft) * Nt;
            tdr = tdr / max(abs([max(tdr), min(tdr)]));
        end
    end
end