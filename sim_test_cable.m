%% Initial settings
clear;
clc;
close all;
format compact, format short;

sp = SavePlot();

%% Transmitted signal creation
fs = 1e9; % [Hz] sampling frequency

% Signal parameters
tau = 200e-9; % [s] burst duration
T = 2000e-9; % [s] bursts spacing
NT = T * fs; % number of samples in T
Ntau = tau * fs; % number of samples in tau

fmin = 10e6; % [Hz] minimum frequency
fmax = 60e6; % [Hz] maximum frequency
delta_f = 0.5e6; % [Hz] frequency increment

Nf = (fmax-fmin)/delta_f + 1; % number of bursts

freqs = (fmin:delta_f:fmax)'; % [Hz] frequencies of the signal
omega = 2*pi*freqs; % [rad]

% Transmitted signal
[Vin, t] = sf_sin_signal(fs, tau, T, fmin, delta_f, fmax);

%% Cable model
coax_parameters; % load coaxial cable parameters

l = 50; % [m] cable length

line_model = struct( ...
    'linemodel', 'coax', 'ri', ri, 'ro', ro, 't', thickness, 'l', l, ... % Line model and geometry
    'epsilon_r', epsilon_r, 'esrtoggle', true, 'tan_d', tan_d, 'debyetoggle', false, ... % Dielectric properties
    'sigma', sigma, 'skintoggle', true, ... % Conductor properties
    'Lprofile', struct('shape', 'disabled'), ...
    'Rprofile', struct('shape', 'disabled'), ...
    'Cprofile', struct('shape', 'disabled'), ...
    'Gprofile', struct('shape', 'disabled') ...
    );

line = TLine;
line.name = 'rg58_disp_50m_load_inf';
line.add_line(line_model);
line.Zsource = Z0_coax;
line.Zmeas = @(w) inf;
line.Zload = @(w) inf;

%% "Measured" signal
Vmeas = real(line.get_tdr(t, Vin));

sp.new('test_cable_reflectogram');
% 10 MHz reflectogram
n = (0 : 0.5 * NT - 1)';
subplot(3, 1, 1);
plot(t(n + 1) * 1e6, Vmeas(n + 1));
xlabel('Time [$\mu s$]')
ylabel('10 MHz')

% 35 MHz reflectogram
n = (50 * NT : 50.5 * NT - 1)';
subplot(3, 1, 2);
plot(t(n + 1) * 1e6, Vmeas(n + 1));
xlabel('Time [$\mu s$]')
ylabel('35 MHz')

% 60 MHz reflectogram
n = (100 * NT : 100.5 * NT - 1)';
subplot(3, 1, 3);
plot(t(n + 1) * 1e6, Vmeas(n + 1));
xlabel('Time [$\mu s$]')
ylabel('60 MHz')

%% Propagation and reflection filter estimation
[tofs, mag_H, phi_H] = ranging_and_filter_estimation(Vmeas, fs, T, tau, freqs);

v_line = 2 * l ./ tofs;

%% alpha estimation
A = -2*l*[ones(size(omega)) omega 1/2*omega.^2];

% Scaling matrix
B = [
    1     0     0;
    0     1e-6  0;
    0     0     1e-12;
    ];
A_MHz = A*B;

alpha_hat = B * (A_MHz\log(mag_H));

figure
plot(freqs/1e6, log(mag_H), freqs/1e6, A*alpha_hat)
xlabel("Frequency [MHz]")
ylabel("$\ln |H(\omega)|$")
legend("Filter estimation", "Fitting")

%% beta estimation
beta_hat = B * (A_MHz\phi_H);

beta = imag(gamma_coax(omega));

figure
plot(freqs/1e6, phi_H, freqs/1e6, A*beta_hat)
xlabel("Frequency [MHz]")
ylabel("$\angle H(\omega)$")
legend("Filter estimation", "Fitting")

%% velocity
v_line_hat = mean(v_line);

sp.new('velocity');
plot(...
    freqs/1e6, omega./imag(gamma_coax(omega)),...
    freqs/1e6, v_line,...
    freqs/1e6, omega./(phi_H/(-2*l)),...
    freqs/1e6, omega./(A*beta_hat/(-2*l)))
xlabel('Frequency [MHz]')
ylabel('Phase velocity [m/s]')
legend(...
    'Theoretical ($\omega/\beta(\omega)$)',...
    'Estimation (from time of flight)',...
    'Estimation (from estimated filter)',...
    'Estimation (from the fitted filter)')

%% Print computed parameters
table(alpha_hat)
table(beta_hat)
table(v_line_hat)

%% Save useful variables
alpha0 = alpha_hat(1);
beta0 = beta_hat(1);
v_line = v_line_hat;
save('cache/coax_params_10MHz_60MHz.mat', 'alpha0', 'beta0', 'v_line');

%% Transmitted signal creation (100 MHz - 150 MHz)
fs = 3e9; % [Hz] sampling frequency

% Signal parameters
tau = 20e-9; % [s] burst duration
T = 1000e-9; % [s] bursts spacing
NT = T * fs; % number of samples in T
Ntau = tau * fs; % number of samples in tau

fmin = 100e6; % [Hz] minimum frequency
fmax = 150e6; % [Hz] maximum frequency
delta_f = 0.5e6; % [Hz] frequency increment

Nf = (fmax-fmin)/delta_f + 1; % number of bursts

freqs = (fmin:delta_f:fmax)'; % [Hz] frequencies of the signal
omega = 2*pi*freqs; % [rad]

% Transmitted signal
[Vin, t] = sf_sin_signal(fs, tau, T, fmin, delta_f, fmax);

%% Cable model (100 MHz - 150 MHz)
line = TLine;
line.name = 'rg58_disp_50m_load_inf_hf';
line.add_line(line_model);
line.Zsource = Z0_coax;
line.Zmeas = @(w) inf;
line.Zload = @(w) inf;

%% "Measured" signal
Vmeas = real(line.get_tdr(t, Vin));

%% Propagation and reflection filter estimation
[tofs, mag_H, phi_H] = ranging_and_filter_estimation(Vmeas, fs, T, tau, freqs);

v_line = 2 * l ./ tofs;

%% alpha estimation
A = -2*l*[ones(size(omega)) omega 1/2*omega.^2];
A_MHz = A*B;

alpha_hat = B * (A_MHz\log(mag_H));

%% beta estimation
beta_hat = B * (A_MHz\phi_H);

%% Print computed parameters
table(alpha_hat)
table(beta_hat)
table(v_line_hat)

%% Save useful variables
alpha0 = alpha_hat(1);
beta0 = beta_hat(1);
v_line = mean(v_line);
save('cache/coax_params_100MHz_150MHz.mat', 'alpha0', 'beta0', 'v_line');

%% Save figures
sp.save()