%% Initial settings
clear;
clc;
close all;
format compact, format short;

sp = SavePlot();

%% Stepped-frequency signal parameters
fs = 3e9; % [Hz] sampling frequency

% Signal parameters
tau = 100e-9; % [s] burst duration
T = 2000e-9; % [s] bursts spacing

fmin = 10e6; % [Hz] minimum frequnecy
fmax = 50e6; % [Hz] maximum frequency
delta_f = 10e6; % [Hz] frequency increment
Nf = (fmax - fmin) / delta_f + 1; % Number of frequencies

%% Propagation and reflection filter (open end)
coax_parameters; % gamma_coax
l = 100; % [m] cable length
Gamma = 1; % reflection coefficient

H = @(w) exp(-gamma_coax(w)*2*l) * Gamma;

%% OLS estimation of the sinusoids
fig = sp.new('ols_trend_coax');
fig.width = 15.5;
fig.height = 17;

for i = 0:Nf-1
    f = fmin + delta_f * i;
    F = f/fs;
    
    [Vin, t] = sf_sin_signal(fs, tau, T, f); % input signal
    Vout = real(fft_filter_response(fs, Vin, H)); % filtered signal
    
    subplot(5, 1, i + 1)
    plot(t, Vout)
    
    n = round((0.535*T*fs : 0.545*T*fs)');
    
    [d, phi, c0, a, b, m] = ols_sin_estimation(fs, f, Vout, n, true);
    
    err_phi = sprintf('$e_{\\angle H} = %.2f^\\circ$', (rad2deg(wrapToPi(phi + pi/2) - angle(H(2*pi*f)))));
    err_mag = sprintf('$er_{|H|} = $ %.2e \\%%', (d - abs(H(2*pi*f))) ./ abs(H(2*pi*f)) * 100);
    
    ylabel(sprintf('$\\omega = %d $ MHz', f/1e6))
    
    wt = 2 * pi * F * n;
    
    A = [ones(size(wt)) cos(wt) -sin(wt) n/fs];
    
    hold on
    plot(n / fs, A*[c0 a b m]')
    xlabel('Time [s]')
    xlim([0.48*T 0.8*T])
    annotation('textbox', [0.6 -0.03+(Nf-i)*0.171 0.1 0.1], 'String', {err_phi, err_mag}, 'FitBoxToText','on','BackgroundColor', 'w','FontSize',8);
end

%%
sp.new('ols_trend_coax_p');

plot(t, Vout)
hold on
n = round((0.535*T*fs : 0.545*T*fs)');

plot(n / fs, A*[c0 a b m]')
xlabel('Time [s]')
xlim([0.48*T 0.6*T])
annotation('textbox', [0.7 0.7 0.1 0.1], 'String', {err_phi, err_mag}, 'FitBoxToText','on','BackgroundColor', 'w','FontSize',8);

%%
sp.new('reflected_burst');

plot(t, Vout)
xlim([0.48*T 0.6*T])
xlabel('Time [s]')

%% Save figures
sp.save()