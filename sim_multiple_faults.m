%% Initial settings
clear;
clc;
close all;
format compact, format short;

sp = SavePlot();

%% Transmitted signal creation
fs = 3e9; % [Hz] sampling frequency

% Signal parameters
tau = 20e-9; % [s] burst duration
T = 1000e-9; % [s] bursts spacing
NT = T * fs; % number of samples in T
Ntau = tau * fs; % number of samples in tau

fmin = 100e6; % [Hz] minimum frequency
fmax = 150e6; % [Hz] maximum frequency
delta_f = 0.5e6; % [Hz] frequency increment

Nf = (fmax-fmin)/delta_f + 1; % number of bursts

freqs = (fmin:delta_f:fmax)'; % [Hz] frequencies of the signal
omega = 2*pi*freqs; % [rad]

% Transmitted signal
[Vin, t] = sf_sin_signal(fs, tau, T, fmin, delta_f, fmax);

%% Parameters definition
coax_parameters; % load coaxial cable parameters

Z0 = 50; % [ohm] theoretical characteristic impedance

load('cache/coax_params_100MHz_150MHz.mat');

l = 50; % [m] cable length

% Faults
ZF = [50 50 200]'; % [ohm] fault impedance
fault_type = logical([0 1 0]); % 0 = series, 1 = parallel
l_fault = [15 25 30]'; % [m] fault position

% Theoretical reflection coefficients
Gamma_ser = ZF ./ (ZF + 2 * Z0);
Gamma_par = -Z0 ./ (Z0 + 2 * ZF);
Gamma = NaN(size(ZF));
Gamma(~fault_type) = Gamma_ser(~fault_type);
Gamma(fault_type) = Gamma_par(fault_type);

%% Cable model
line_model = struct( ...
    'linemodel', 'coax', 'ri', ri, 'ro', ro, 't', thickness, 'l', l, ... % Line model and geometry
    'epsilon_r', epsilon_r, 'esrtoggle', true, 'tan_d', tan_d, 'debyetoggle', false, ... % Dielectric properties
    'sigma', sigma, 'skintoggle', true, ... % Conductor properties
    'Lprofile', struct('shape', 'disabled'), ...
    'Rprofile', struct('shape', 'disabled'), ...
    'Cprofile', struct('shape', 'disabled'), ...
    'Gprofile', struct('shape', 'disabled') ...
    );

line = TLine;
line.name = 'rg58_disp_multiple_faults_50m';
line.add_line(line_model);
line.Zsource = Z0_coax;
line.Zmeas = @(w) inf;
line.Zload = Z0_coax;

line.lumped_series_fault_pos = l_fault(~fault_type);
line.lumped_series_fault_Z = ZF(~fault_type);
line.lumped_parallel_fault_pos = l_fault(fault_type);
line.lumped_parallel_fault_Z = ZF(fault_type);

%% "Measured" signal
Vmeas = real(line.get_tdr(t, Vin));

%% Propagation and reflection filter estimation
[tofs, mag_H, phi_H] = ranging_and_filter_estimation(Vmeas, fs, T, tau, freqs);

l_faults = v_line * tofs / 2;

l_fault_hat = mean(l_faults)'; % line length estimation

Gamma_eq = NaN(size(l_fault_hat));

% Scaling matrix
B = [
    1     0     0;
    0     1e-6  0;
    0     0     1e-12;
    ];

for i=1:length(l_fault_hat)
    %% alpha estimation
    A = -2 * l_fault_hat(i) * [ones(size(omega)) omega 1/2*omega.^2];    
    A_MHz = A * B;

    alpha_hat = B * (A_MHz \ log(mag_H(:, i)));

    %% beta estimation
    beta_hat = B * (A_MHz \ phi_H(:, i));

    %% Gamma estimation
    mod_Gamma_hat = exp(-2 * l_fault_hat(i) * (alpha_hat(1) - alpha0));
    phi_Gamma_hat = wrapTo2Pi(-2 * l_fault_hat(i) * (beta_hat(1) - beta0));
    Gamma_eq(i) = mod_Gamma_hat * exp(1i * phi_Gamma_hat); %estimated reflection coefficient
end

%% Effective Gamma
Gamma_hat = gamma_estimation(Gamma_eq, Z0); % reflection coefficient estimation for each fault

%% Estimation errors
er_l_fault_pc = (l_fault_hat - l_fault) ./ l_fault * 100;
disp(table(ZF, l_fault_hat, er_l_fault_pc));

mod_Gamma = abs(Gamma_hat);
phase_Gamma = rad2deg(angle(Gamma_hat));
er_mod_Gamma_pc = (abs(Gamma_hat) - abs(Gamma)) ./ abs(Gamma) * 100;
e_phase_Gamma = rad2deg(angle(Gamma_hat) - angle(Gamma));

disp(table(ZF, mod_Gamma, er_mod_Gamma_pc, phase_Gamma, e_phase_Gamma))

%% Second cable
% Faults
ZF = [200 50 10]'; % [ohm] fault impedance
fault_type = logical([1 0 1]); % 0 = series, 1 = parallel
l_fault = [10 20 35]'; % [m] fault position

% Theoretical reflection coefficients
Gamma_ser = ZF ./ (ZF + 2 * Z0);
Gamma_par = -Z0 ./ (Z0 + 2 * ZF);
Gamma = NaN(size(ZF));
Gamma(~fault_type) = Gamma_ser(~fault_type);
Gamma(fault_type) = Gamma_par(fault_type);

%% Cable model
line = TLine;
line.name = 'rg58_disp_multiple_faults_50m_2';
line.add_line(line_model);
line.Zsource = Z0_coax;
line.Zmeas = @(w) inf;
line.Zload = Z0_coax;

line.lumped_series_fault_pos = l_fault(~fault_type);
line.lumped_series_fault_Z = ZF(~fault_type);
line.lumped_parallel_fault_pos = l_fault(fault_type);
line.lumped_parallel_fault_Z = ZF(fault_type);

%% "Measured" signal
Vmeas = real(line.get_tdr(t, Vin));

%% Propagation and reflection filter estimation
[tofs, mag_H, phi_H] = ranging_and_filter_estimation(Vmeas, fs, T, tau, freqs);

l_faults = v_line * tofs / 2;

l_fault_hat = mean(l_faults)'; % line length estimation

Gamma_eq = NaN(size(l_fault_hat));

% Scaling matrix
B = [
    1     0     0;
    0     1e-6  0;
    0     0     1e-12;
    ];

for i=1:length(l_fault_hat)
    %% alpha estimation
    A = -2 * l_fault_hat(i) * [ones(size(omega)) omega 1/2*omega.^2];    
    A_MHz = A * B;

    alpha_hat = B * (A_MHz \ log(mag_H(:, i)));

    %% beta estimation
    beta_hat = B * (A_MHz \ phi_H(:, i));

    %% Gamma estimation
    mod_Gamma_hat = exp(-2 * l_fault_hat(i) * (alpha_hat(1) - alpha0));
    phi_Gamma_hat = wrapTo2Pi(-2 * l_fault_hat(i) * (beta_hat(1) - beta0));
    Gamma_eq(i) = mod_Gamma_hat * exp(1i * phi_Gamma_hat); %estimated reflection coefficient
end

%% Effective Gamma
Gamma_hat = gamma_estimation(Gamma_eq, Z0); % reflection coefficient estimation for each fault

%% Estimation errors
er_l_fault_pc = (l_fault_hat - l_fault) ./ l_fault * 100;
disp(table(ZF, l_fault_hat, er_l_fault_pc));

mod_Gamma = abs(Gamma_hat);
phase_Gamma = rad2deg(angle(Gamma_hat));
er_mod_Gamma_pc = (abs(Gamma_hat) - abs(Gamma)) ./ abs(Gamma) * 100;
e_phase_Gamma = rad2deg(angle(Gamma_hat) - angle(Gamma));

disp(table(ZF, mod_Gamma, er_mod_Gamma_pc, phase_Gamma, e_phase_Gamma))

%% Save figures
sp.save()