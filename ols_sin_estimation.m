function [d, phi, c0, a, b, m] = ols_sin_estimation(fs, f, y, n, trend)
%ols_sin_estimation OLS estimation of a sinusoid's parameters
%   Detailed explanation goes here

if nargin == 4
    trend = false;
end

F = f/fs; % digital frequency

y_ols = y(n + 1); % signal used for OLS estimation

t = n / fs;
wt = 2 * pi * F * n;

if trend
    A = [ones(size(wt)) cos(wt) -sin(wt) t]; % sinusoid + trend
else
    A = [ones(size(wt)) cos(wt) -sin(wt)]; % sinusoid only
    m = 0;
end

p = A\y_ols;

c0 = p(1);
a = p(2);
b = p(3);

if trend
    m = p(4);
end

d = sqrt(a^2 + b^2);
phi = atan2(b, a);

end