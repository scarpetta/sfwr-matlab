function Gamma = gamma_estimation(Gamma_eq, Z0)
%gamma_estimation iteratively compute the effective Gamma from equivalent Gamma
%   Detailed explanation goes here

Gamma = NaN(size(Gamma_eq));
ZFs = NaN(size(Gamma_eq));
Ts = NaN(size(Gamma_eq));

Gamma(1) = Gamma_eq(1);

for i = 2:length(Gamma_eq)
    if real(Gamma(i-1)) > 0
        ZFs(i-1) = (2*Z0 * Gamma(i-1))/(1-Gamma(i-1));
    else
        ZFs(i-1) = (Z0* (1+Gamma(i-1)))/(-2*Gamma(i-1));
    end
    
    if real(Gamma(i-1)) > 0
        Ts(i-1) = (2*Z0)/(ZFs(i-1) + 2*Z0);
    else
        Ts(i-1) = (2*ZFs(i-1))/(2*ZFs(i-1) + Z0);
    end
    
    Gamma(i) = Gamma_eq(i)/prod(Ts(1:i-1).^2);
end
end