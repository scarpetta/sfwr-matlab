%% Initial settings
clear;
clc;
close all;
format compact, format short;

sp = SavePlot();

%% Stepped-frequency signal creation
fs = 3e9; % [Hz] sampling frequency

% Signal parameters
tau = 500e-9; % [s] burst duration
T = 2000e-9; % [s] bursts spacing

f = 10e6; % [Hz] sinusoid frequency
F = f/fs; % digital frequency

% Transmitted signal
[Vin, t] = sf_sin_signal(fs, tau, T, f); % d = 1, phi = -pi / 2

%% Estimation errors for various tau_c / tau ratios
tau_ratio = logspace(-2, 2, 5)'; % tau_c / tau ratios
estimation_range = [0.6 0.9]; % relative time range for OLS estimation

er_mag_pc = nan(size(tau_ratio)); % [%] magnitude relative estimation errors
e_phase = nan(size(tau_ratio)); % [deg] phase estimation errors

for i = 1 : length(tau_ratio)
    tau_c = tau_ratio(i) * tau; % [s] time constant
    wc = 1/tau_c; % [rad] cutoff frequency
    
    Hlp = @(w) 1./(1 + 1i*w/wc); % low-pass filter
    
    % Filtered signal
    Vout = real(fft_filter_response(fs, Vin, Hlp));
    
    % Indexes for OLS estimation
    n = (round(estimation_range(1) * tau * fs) : round(estimation_range(2) * tau * fs))';
    
    [d, phi, ~] = ols_sin_estimation(fs, f, Vout, n, true);
    
    % Errors computation
    er_mag_pc(i) = (d - abs(Hlp(2 * pi * f))) ./ abs(Hlp(2 * pi * f)) * 100;
    e_phase(i) = rad2deg(wrapToPi(phi + pi / 2) - angle(Hlp(2 * pi * f)));
end

table(tau_ratio, er_mag_pc, e_phase)

%% Plot of three fittings
tau_ratio = logspace(-1, 1, 3)'; % tau_c / tau ratios

fig = sp.new('sine_fitting_lp');
fig.width = 12;
fig.height = 10;

for i = 1 : length(tau_ratio)
    tau_c = tau_ratio(i) * tau; % [s] time constant
    wc = 1/tau_c; % [rad] cutoff frequency
    
    Hlp = @(w) 1./(1 + 1i*w/wc); % low-pass filter
    
    % Filtered signal
    Vout = real(fft_filter_response(fs, Vin, Hlp));
    
    % Indexes for OLS estimation
    n = (round(estimation_range(1) * tau * fs) : round(estimation_range(2) * tau * fs))';
    
    [d, phi, c0, a, b, m] = ols_sin_estimation(fs, f, Vout, n, true);
    
    subplot(length(tau_ratio), 1, i)
    plot(t, Vout)
    hold on
    plot(n / fs, c0 + m * n / fs + d * cos(2 * pi * F * n + phi))
    xlabel('Time [s]')
    ylabel(sprintf('$\\tau_c / \\tau = %.1f$', tau_ratio(i)))
end

%% Save figures
sp.save()