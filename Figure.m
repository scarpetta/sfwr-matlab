classdef Figure < handle
    properties
        name;
        figure;
        width;
        height;
    end
    methods
        function obj = Figure(name)
            obj.name = name;
            obj.figure = figure;
        end
    end
end