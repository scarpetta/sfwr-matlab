epsilon0 = 8.854187817e-12; % Vacuum dielectric permittivity
mu0 = 4*pi*1e-7; % Vacuum magnetic permeability

sigma = 5.96e7; % Copper conductance
ro = 1.475e-3; % outer conductor radius [m]
ri = 0.455e-3; % inner conductor radius [m]
thickness = 0.007e-3; % outer conductor thickness [m]
epsilon_r = 2.26; % relative dielectric permittivity of the inner material
tan_d = 0.00031; % dielectric loss tangent

R0_line = 1/(pi*ri^2*sigma) + 1/(pi*sigma)/((ro + thickness)^2 - ro^2); % Distributed resistance [ohm/m]
L0_line = mu0/(2*pi) * log(ro/ri); % Distributed inductance [H/m]
G0_line = 0; % Distributed conductance [S/m]
C0_line = 2*pi*epsilon0*epsilon_r / log(ro/ri); % Distributed capacitance [F/m]

% Skin effect
Rsheet = @(w) sqrt(w*mu0/(2*sigma));
R = @(w) R0_line + Rsheet(w) / (2*pi) * (1/ri + 1/ro);
L = @(w) L0_line + Rsheet(w) ./ (2*pi*w) * (1/ri + 1/ro);

% ESR (tan_delta)
G = @(w) G0_line + tan_d*w*C0_line;
C = @(w) C0_line;

% Propagation constant
gamma_coax = @(w) sqrt((R(w) + 1i*w.*L(w)) .* (G(w) + 1i*w.*C(w)));

% Characteristic impedance
Z0_coax = @(w) sqrt((R(w) + 1i*w.*L(w)) ./ (G(w) + 1i*w.*C(w)));