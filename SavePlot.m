classdef SavePlot < handle
    properties
        format = 'svg'
        path = '../figures/'
        width = 15.5; % [cm]
        height = 9; % [cm]
        figures = {}
    end
    methods
        function obj = SavePlot()
            set(groot, 'defaultLineLineWidth', 0.5);
            set(groot ,'defaultAxesFontSize', 12);
            set(groot, 'defaultLegendLocation', 'best');
            set(groot, 'defaulttextinterpreter', 'latex');
            set(groot, 'defaultAxesTickLabelInterpreter', 'latex');
            set(groot, 'defaultLegendInterpreter', 'latex');
            set(groot, 'DefaultTextboxshapeInterpreter', 'latex')
            set(groot, 'defaultTextFontName', 'Times');
            set(groot, 'defaultAxesFontName', 'Times');
            set(groot, 'defaultLegendFontName', 'Times');
        end
        
        function fig = new(obj, name)
            fig = Figure(name);
            fig.width = obj.width;
            fig.height = obj.height;
            obj.figures(end + 1, :) = {fig};
        end
        
        function save(obj)
            [~, ~, ~] = mkdir(obj.path);
            
            for i = 1:size(obj.figures)
                fig = obj.figures{i};
                set(fig.figure,...
                    'PaperPositionMode', 'manual',...
                    'PaperPosition', [0.25, 0.25, fig.width - 0.5, fig.height - 0.5],...
                    'PaperUnits', 'centimeters',...
                    'PaperSize', [fig.width, fig.height]);
                
                saveas(fig.figure,...
                    strcat(obj.path, fig.name, '.', obj.format));
            end
        end
    end
end