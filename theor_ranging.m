%% Initial settings
clear;
clc;
close all;
format compact, format short;

sp = SavePlot();

%% Transmitted signal creation
fs = 0.5e9; % [Hz] sampling frequency

% Signal parameters
tau = 100e-9; % [s] burst duration
T = 2000e-9; % [s] bursts spacing

f = 20e6; % [Hz] sinusoid frequency

% Transmitted signal
[Vin, t] = sf_sin_signal(fs, tau, T, f);

%% Propagation and reflection filter (open end)
coax_parameters; % gamma_coax
l = 100; % [m] cable length
Gamma = 1; % reflection coefficient

H = @(w) exp(-gamma_coax(w) * 2 * l) * Gamma;

%% "Measured" signal
Vout = Vin + real(fft_filter_response(fs, Vin, H));

%% Transmitted and reflected signal
% Tansmitted signal
Vt = Vout;
Vt(tau * fs : end) = 0;

% Reflected signal
Vr = Vout;
Vr(1 : tau * fs) = 0;

sp.new('ranging_meas_signal');
plot(t, Vout)
xlabel('Time [s]')

sp.new('ranging_tx_rx');
subplot(2, 1, 1)
plot(t, Vt)
ylabel('Transmitted')
xlabel('Time [s]')
subplot(2, 1, 2)
plot(t, Vr)
ylabel('Reflected')
xlabel('Time [s]')

%% Cross-correlation
sp.new('ranging_xcorr');
[corr, lag] = xcorr(Vr, Vt);

[~, I] = max(abs(corr));
tof = lag(I);
plot(lag / fs, corr)
hold on
plot(tof / fs, corr(I), 'o')
xlabel('Delay [s]')
legend('Cross-correlation', 'Reflected burst start')

%% Anticipated reflected burst figure
sp.new('ranging_translated');
plot(t, Vout)
hold on
plot(tof / fs, Vr(tof + 1), '*')
plot((0 : tau * fs - 1) / fs, Vr(tof + (0 : tau * fs - 1) + 1))
legend('Measured signal', 'Reflected burst starting point', 'Anticipated reflected burst')
xlabel('Time [s]')

%% Propagation and reflection filter (short circuit end)
Gamma = -1; % reflection coefficient

H = @(w) exp(-gamma_coax(w) * 2 * l) * Gamma;

%% "Measured" signal
Vout = Vin + real(fft_filter_response(fs, Vin, H));

%% Transmitted and reflected signal
% Tansmitted signal
Vt = Vout;
Vt(tau * fs : end) = 0;

% Reflected signal
Vr = Vout;
Vr(1 : tau * fs) = 0;

%% Cross-correlation
sp.new('ranging_xcorr_sc');
[corr, lag] = xcorr(Vr, Vt);

[~, I] = max(abs(corr));
tof = lag(I);
plot(lag / fs, corr)
hold on
plot(tof / fs, corr(I), 'o')
xlabel('Delay [s]')
legend('Cross-correlation', 'Reflected burst start')

sp.new('ranging_translated_sc');
plot(t, Vout)
hold on
plot(tof / fs, Vr(tof + 1), '*')
plot((0 : tau * fs - 1) / fs, Vr(tof + (0 : tau * fs - 1) + 1))
legend('Measured signal', 'Reflected burst starting point', 'Anticipated reflected burst')
xlabel('Time [s]')

%% Save figures
sp.save()