%% Initial settings
clear;
clc;
close all;
format compact, format short;

sp = SavePlot();

%% Transmitted signal creation
fs = 1e9; % [Hz] sampling frequency

% Signal parameters
tau = 200e-9; % [s] burst duration
T = 2000e-9; % [s] bursts spacing
NT = T * fs; % number of samples in T
Ntau = tau * fs; % number of samples in tau

fmin = 10e6; % [Hz] minimum frequency
fmax = 60e6; % [Hz] maximum frequency
delta_f = 0.5e6; % [Hz] frequency increment

Nf = (fmax-fmin)/delta_f + 1; % number of bursts

freqs = (fmin:delta_f:fmax)'; % [Hz] frequencies of the signal
omega = 2*pi*freqs; % [rad]

% Transmitted signal
[Vin, t] = sf_sin_signal(fs, tau, T, fmin, delta_f, fmax);

%% Parameters definition
coax_parameters; % load coaxial cable parameters

Z0 = 50; % [ohm] theoretical characteristic impedance

load('cache/coax_params_10MHz_60MHz.mat');

l = 100; % [m] cable length

ZFs = [20 50 100 200 500]'; % [ohm] series fault impedance
ZFp = [5 10 20 50 200]'; % [ohm] parallel fault impedance
ZF = [ZFs; ZFp];

fault_type = [zeros(size(ZFs)); ones(size(ZFp))]; % 0 = series, 1 = parallel

% Theoretical reflection coefficients
Gamma_ser = ZFs ./ (ZFs + 2 * Z0);
Gamma_par = -Z0 ./ (Z0 + 2 * ZFp);
Gamma = [Gamma_ser; Gamma_par];

l_fault = [30 * ones(size(ZF)); 70 * ones(size(ZF))]; % [m] fault position
ZF = [ZF; ZF];
Gamma = [Gamma; Gamma];
fault_type = [fault_type; fault_type];

Gamma_hat = NaN(size(ZF)); % reflection coefficient estimation for each fault
l_fault_hat = NaN(size(ZF)); % cable length estimation for each fault

%% Simulations and estimation
% Scaling matrix
B = [
    1     0     0;
    0     1e-6  0;
    0     0     1e-12;
    ];

for j = 1:length(ZF)
    %% Cable model
    line_model = struct( ...
        'linemodel', 'coax', 'ri', ri, 'ro', ro, 't', thickness, 'l', l, ... % Line model and geometry
        'epsilon_r', epsilon_r, 'esrtoggle', true, 'tan_d', tan_d, 'debyetoggle', false, ... % Dielectric properties
        'sigma', sigma, 'skintoggle', true, ... % Conductor properties
        'Lprofile', struct('shape', 'disabled'), ...
        'Rprofile', struct('shape', 'disabled'), ...
        'Cprofile', struct('shape', 'disabled'), ...
        'Gprofile', struct('shape', 'disabled') ...
    );
    
    line = TLine;
    line.add_line(line_model);
    line.Zsource = Z0_coax;
    line.Zmeas = @(w) inf;
    line.Zload = Z0_coax;
    
    if fault_type(j) == 0
        line.name = sprintf('rg58_disp_100m_%dpc_ser_%d_ohm', l_fault(j), ZF(j));
        line.lumped_series_fault_pos = l_fault(j) / l;
        line.lumped_series_fault_Z = {@(w) ZF(j)};
    else
        line.name = sprintf('rg58_disp_100m_%dpc_par_%d_ohm', l_fault(j), ZF(j));
        line.lumped_parallel_fault_pos = l_fault(j) / l;
        line.lumped_parallel_fault_Z = {@(w) ZF(j)};
    end

    %% "Measured" signal
    Vmeas = real(line.get_tdr(t, Vin));

    %% Propagation and reflection filter estimation
    [tofs, mag_H, phi_H] = ranging_and_filter_estimation(Vmeas, fs, T, tau, freqs);

    l_faults = v_line * tofs / 2;

    l_fault_hat(j) = mean(l_faults); % line length estimation

    %% alpha estimation
    A = -2 * l_fault_hat(j) * [ones(size(omega)) omega 1/2*omega.^2];
    A_MHz = A * B;
    
    alpha_hat = B * (A_MHz \ log(mag_H));

    %% beta estimation
    beta_hat = B * (A_MHz \ phi_H);

    %% Gamma estimation
    mod_Gamma_hat = exp(- 2 * l_fault_hat(j) * (alpha_hat(1) - alpha0));
    phi_Gamma_hat = wrapTo2Pi(-2 * l_fault_hat(j) * (beta_hat(1) - beta0));

    Gamma_hat(j) = mod_Gamma_hat * exp(1i * phi_Gamma_hat); %estimated reflection coefficient
end

%% Plot of the estimated reflection coefficients in complex plane
sp.new('Gamma_30m');
hold on;

rG = real(Gamma_hat);
iG = imag(Gamma_hat);

legends = cell(size(Gamma_hat));

for i = 1 : length(Gamma_hat)
    if fault_type(i) == 0
        plot(rG(i), iG(i), 'o')
        legends{i} = sprintf('$Z_S = %d \\Omega$ (%d m)', ZF(i), l_fault(i));
    else
        plot(rG(i), iG(i), '^')
        legends{i} = sprintf('$Z_P = %d \\Omega$ (%d m)', ZF(i), l_fault(i));
    end
end

legend(legends)

grid
ylim([-0.2 1])
xlim([-1 1])
xlabel('$\Re \left\lbrace \Gamma \right\rbrace $')
ylabel('$\Im \left\lbrace \Gamma \right\rbrace $')

%% Estimation errors
er_l_fault_pc = (l_fault_hat - l_fault) ./ l_fault * 100;
disp(table(ZF, l_fault_hat, er_l_fault_pc));

mod_Gamma = abs(Gamma_hat);
phase_Gamma = rad2deg(angle(Gamma_hat));
er_mod_Gamma_pc = (abs(Gamma_hat) - abs(Gamma)) ./ abs(Gamma) * 100;
e_phase_Gamma = rad2deg(angle(Gamma_hat) - angle(Gamma));

disp(table(ZF, mod_Gamma, er_mod_Gamma_pc, phase_Gamma, e_phase_Gamma))

%% Save figures
sp.save()