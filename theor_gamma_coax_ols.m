%% Initial settings
clear;
clc;
close all;
format compact, format short;

sp = SavePlot();

%% Transmitted signal creation
fs = 1e9; % [Hz] sampling frequency

% Signal parameters
tau = 200e-9; % [s] burst duration
T = 2000e-9; % [s] bursts spacing

fmin = 10e6; % [Hz] minimum frequency
fmax = 60e6; % [Hz] maximum frequency
delta_f = 0.5e6; % [Hz] frequency increment

Nf = (fmax-fmin)/delta_f + 1; % number of bursts

freqs = (fmin:delta_f:fmax)'; % [Hz] frequencies of the signal
omega = 2*pi*freqs; % [rad]

% Transmitted signal
[Vin, t] = sf_sin_signal(fs, tau, T, fmin, delta_f, fmax);

%% Theoretical propagation constant of the cable
coax_parameters; % gamma_coax

alpha = real(gamma_coax(omega));
beta = imag(gamma_coax(omega));

%% Estimation of alpha_{1,2}
A1 = [omega 1/2*omega.^2];

B1 = [
    1e-6  0;
    0     1e-12;
    ];

alpha_par_no_alpha0 = B1*((A1*B1)\alpha);

sp.new('alpha_coax_no_alpha0');
plot(freqs/1e6, alpha, freqs/1e6, A1*alpha_par_no_alpha0)
xlabel('Frequency [MHz]')
legend('Theoretical $\alpha$', '$\alpha$ estimation (model without $\alpha_0$)')

%% Estimation of alpha_{0,1,2}
A2 = [ones(size(omega)) omega 1/2*omega.^2];

B2 = [
    1     0     0;
    0     1e-6  0;
    0     0     1e-12;
    ];

alpha_par = B2 * (A2*B2\alpha);

sp.new('alpha_coax_estimation');
plot(freqs/1e6, alpha, freqs/1e6, A2*alpha_par)
xlabel('Frequency [MHz]')
legend('Theoretical $\alpha$', '$\alpha$ estimation (model with $\alpha_0$)')

%% Estimation of beta_{1,2}
beta_par_no_alpha0 = (A1\beta);

sp.new('beta_coax_estimation');
plot(freqs/1e6, beta, freqs/1e6, A1*beta_par_no_alpha0)
xlabel('Frequency [MHz]')
legend('Theoretical $\beta$', '$\beta$ estimation (model without $\beta_0$)')

%% Save figures
sp.save()