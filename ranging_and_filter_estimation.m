function [tofs, magH, phiH] = ranging_and_filter_estimation(Vmeas, fs, T, tau, freqs)
%ranging_and_filter_estimation Faults detection and ranging, filter estimation
%   Detailed explanation goes here

estimation_range = [0.6 0.9]; % relative time range for OLS estimation

magH = []; % estimated filter magnitude
phiH = []; % estimated filter phase
tofs = []; % estimated time of flight for each frequency

NT = T * fs;
Ntau = tau * fs;

n = (0 : NT - 1)';
nb = (0 : Ntau - 1)';

for i = 1 : length(freqs)
    f = freqs(i); % frequency of burst i
    F = f / fs; % digital frequency of burst i
    
    burst_start = NT * (i - 1); % transmitted burst starting index
    Vmeasi = Vmeas(burst_start + n + 1); % measured signal (time slot for f-frequency sinusoid)
    
    Vt = Vmeasi; % transmitted part
    Vt(Ntau + 1 : end) = 0;
    
    Vr = Vmeasi; % reflected part
    Vr(nb + 1) = 0;
    
    [corr, lags] = xcorr(Vr, Vt); % cross-correlation
    
    th = mean(abs(corr)) * 10; % fault detection threshold
    
    j = 1; % fault index
    [~, I] = max(abs(corr)); % cross-correlation peak
    
    while abs(corr(I)) > th
        tofs(i, j) = lags(I); % estimated time of flight [number of samples]
        
        % Transmitted sinusoid fitting
        [d_tx, phi_tx] = ols_sin_estimation(fs, f, Vmeasi, nb);
        
        Vr_burst = Vmeasi(tofs(i, j) + nb + 1); % reflected burst
        
        n_ols = (round(estimation_range(1) * Ntau) : round(estimation_range(2) * Ntau))';
        
        % Reflected sinusoid fitting
        [d_rx, phi_rx] = ols_sin_estimation(fs, f, Vr_burst, n_ols, true);
        phi_rx = phi_rx - 2 * pi * F * tofs(i, j);
        
        % Magnitude and phase of the transfer function H = Vr / Vt
        magH(i, j) = d_rx / d_tx;
        phiH(i, j) = phi_rx - phi_tx;
        
        % Delete reflected burst from cross-correlation
        corr(I - Ntau : I + Ntau) = 0;
        
        % Delete reflected bursts due to multiple reflections
        for k = 1:j-1
            lag_k = find(lags == tofs(i, k));
            a = min(I, lag_k);
            b = max(I, lag_k);
            
            c = b - a;
            
            m = 2;
            d = a + m*c;
            while d + Ntau < length(corr)
                corr(d - Ntau : d + Ntau) = 0;
                m = m + 1;
                d = a + m*c;
            end
        end
        
        % Detect next fault
        j = j + 1;
        [~, I] = max(abs(corr));
    end
end

%% Delete wrongly detected faults
for j = size(tofs, 2) : -1 : 1
    if sum(tofs(:, j) == 0) > 0
        tofs(:, j) = [];
        magH(:, j) = [];
        phiH(:, j) = [];
    end
end

%% Sort faults from the nearest to the farthest
for  i = 1 : length(freqs)
    [tofs(i, :), I] = sort(tofs(i, :));
    magH(i, :) = magH(i, I);
    phiH(i, :) = phiH(i, I);
end

tofs = tofs / fs; % estimated times of flight [s]
end