function Vout = fft_filter_response(fs, Vin, H)
%fft_filter_response Filter time-domain response calculated using FFT
%   Detailed explanation goes here

N = length(Vin);

Nh = floor(N/2); % number of harmonics

w = 2 * pi * fs / N * (0 : Nh)';

Hfft_pos = H(w);
if isnan(Hfft_pos(1))
    Hfft_pos(1) = 1;
end

Hfft = zeros(size(Vin));
Hfft(1 : Nh + 1) = Hfft_pos;
Hfft(end - Nh + 1 : end) = conj(Hfft_pos(end:-1:2));

Vout = ifft(fft(Vin).*Hfft);

end