# sfwr-matlab

## Initialization

Clone the repository:

    git clone https://scarpetta@bitbucket.org/scarpetta/sfwr-matlab.git

Initialize and update the LineLab submodule:

    git submodule init
    git submodule update

## Scripts
`theor_ranging.m`: Demonstration of the cross-correlation method for
detecting and ranging cable faults

`theor_gamma_coax_ols.m`: Fitting of the theoretical propagation constant
of a coaxial cable with the polynomial model

`theor_lp_response_ols`: OLS fitting of the sinusoid contained in the
response of a first order low-pass filter to a sinusoidal burst

`theor_coax_response_ols`: OLS fitting of the sinusoid contained in the
response of the filter describing propagation and reflection inside a
coaxial cable to a sinusoidal burst

`sim_test_cable.m`: application of the modified SFWR algorithm to a test
coaxial cable. This script estimates and saves to a file the propagation
velocity of the cable and `alpha0` and `beta0` parameters. Run this script
before the following ones.

`sim_load.m`: application of the modified SFWR algorithm to an open-end
coaxial cable

`sim_fault.m`: application of the modified SFWR algorithm to a coaxial
cable with one fault

`sim_multiple_faults.m`: application of the modified SFWR algorithm to a
coaxial cable with multiple faults