%% Initial settings
clear;
clc;
close all;
format compact, format short;

sp = SavePlot();

%% Transmitted signal creation
fs = 1e9; % [Hz] sampling frequency

% Signal parameters
tau = 200e-9; % [s] burst duration
T = 2000e-9; % [s] bursts spacing
NT = T * fs; % number of samples in T
Ntau = tau * fs; % number of samples in tau

fmin = 10e6; % [Hz] minimum frequency
fmax = 60e6; % [Hz] maximum frequency
delta_f = 0.5e6; % [Hz] frequency increment

Nf = (fmax-fmin)/delta_f + 1; % number of bursts

freqs = (fmin:delta_f:fmax)'; % [Hz] frequencies of the signal
omega = 2*pi*freqs; % [rad]

% Transmitted signal
[Vin, t] = sf_sin_signal(fs, tau, T, fmin, delta_f, fmax);

%% Parameters definition
coax_parameters; % load coaxial cable parameters

Z0 = 50; % [ohm] theoretical characteristic impedance

load('cache/coax_params_10MHz_60MHz.mat');

l = 100; % [m] cable length
%ZL = [0 10 30 50 100 300 1e12]';
ZL = [0 10 30 100 300 1e12]'; % [ohm] load impedances
Gamma = (ZL - Z0)./(ZL + Z0); % theorical reflection coefficients

alphas = NaN([length(ZL), 2]); % alpha_1 and alpha_2 for each load
betas = NaN([length(ZL), 2]); % beta_1 and beta_2 for each load
Gamma_hat = NaN(size(ZL)); % reflection coefficient estimation for each load
l_line_hat = NaN(size(ZL)); % cable length estimation for each load

%% Simulations and estimation
for j = 1:length(ZL)
    %% Cable model
    line_model = struct( ...
        'linemodel', 'coax', 'ri', ri, 'ro', ro, 't', thickness, 'l', l, ... % Line model and geometry
        'epsilon_r', epsilon_r, 'esrtoggle', true, 'tan_d', tan_d, 'debyetoggle', false, ... % Dielectric properties
        'sigma', sigma, 'skintoggle', true, ... % Conductor properties
        'Lprofile', struct('shape', 'disabled'), ...
        'Rprofile', struct('shape', 'disabled'), ...
        'Cprofile', struct('shape', 'disabled'), ...
        'Gprofile', struct('shape', 'disabled') ...
    );

    line = TLine;
    line.name = sprintf('rg58_disp_100m_load_%d_ohm', ZL(j));
    line.add_line(line_model);
    line.Zsource = Z0_coax;
    line.Zmeas = @(w) inf;
    line.Zload = @(w) ZL(j);

    %% "Measured" signal
    Vmeas = real(line.get_tdr(t, Vin));

    %% Propagation and reflection filter estimation
    [tofs, mag_H, phi_H] = ranging_and_filter_estimation(Vmeas, fs, T, tau, freqs);

    l_line = v_line * tofs / 2;

    l_line_hat(j) = mean(l_line); % line length estimation

    %% alpha estimation
    A = -2 * l_line_hat(j) * [ones(size(omega)) omega 1/2*omega.^2];
    
    % Scaling matrix
    B = [
        1     0     0;
        0     1e-6  0;
        0     0     1e-12;
        ];
    A_MHz = A * B;
    
    alpha_hat = B * (A_MHz \ log(mag_H));
    alphas(j, :) = alpha_hat(2:3)';

    %% beta estimation
    beta_hat = B * (A_MHz \ phi_H);
    betas(j, :) = beta_hat(2:3)';

    %% Gamma estimation
    mod_Gamma_hat = exp(- 2 * l_line_hat(j) * (alpha_hat(1) - alpha0));
    phi_Gamma_hat = wrapTo2Pi(-2 * l_line_hat(j) * (beta_hat(1) - beta0));

    Gamma_hat(j) = mod_Gamma_hat * exp(1i * phi_Gamma_hat); %estimated reflection coefficient
end

%% Plot of the estimated reflection coefficients in complex plane
sp.new('Gamma_load');
rG = real(Gamma_hat);
iG = imag(Gamma_hat);
plot(rG(1), iG(1), 'o')
hold on
plot(rG(2), iG(2), 'o')
plot(rG(3), iG(3), 'o')
plot(rG(4), iG(4), 'o')
plot(rG(5), iG(5), 'o')
plot(rG(6), iG(6), 'o')
%plot(rG(7), iG(7), 'o')
%legend('$Z_L = 0 \Omega$', '$Z_L = 10 \Omega$', '$Z_L = 30 \Omega$', '$Z_L = 50 \Omega$', '$Z_L = 100 \Omega$', '$Z_L = 300 \Omega$', 'Open end')
legend('$Z_L = 0 \Omega$', '$Z_L = 10 \Omega$', '$Z_L = 30 \Omega$', '$Z_L = 100 \Omega$', '$Z_L = 300 \Omega$', 'Open end')
grid
ylim([-0.3 1])
xlim([-1.1 1.1])
xlabel('$\Re \left\lbrace \Gamma \right\rbrace $')
ylabel('$\Im \left\lbrace \Gamma \right\rbrace $')

%% Estimation errors
er_l_line_ppm = (l_line_hat - l) / l * 1e6;
disp(table(ZL, l_line_hat, er_l_line_ppm));

mod_Gamma = abs(Gamma_hat);
phase_Gamma = rad2deg(angle(Gamma_hat));
er_mod_Gamma_pc = (abs(Gamma_hat) - abs(Gamma)) ./ abs(Gamma) * 100;
e_phase_Gamma = rad2deg(angle(Gamma_hat) - angle(Gamma));

disp(table(ZL, mod_Gamma, er_mod_Gamma_pc, phase_Gamma, e_phase_Gamma))

%% Mean value and standard deviation of alpha and beta parameters
alpha_mean = mean(alphas)';
alpha_std = std(alphas)';
beta_mean = mean(betas)';
beta_std = std(betas)';
disp(table(alpha_mean, alpha_std, beta_mean, beta_std));

%% Save figures
sp.save()