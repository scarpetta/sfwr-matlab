function [V, t] = sf_sin_signal(fs, tau, T, fmin, delta_f, fmax)
%sf_sin_signal Stepped-frequency sinusoidal signal
%   Detailed explanation goes here

Ts = 1/fs; % sampling time

if nargin == 6
    Nf = round((fmax-fmin)/delta_f) + 1; % number of bursts
elseif nargin == 4
    Nf = 1; % number of bursts
    delta_f = 0;
else
    error('Incorrect number of input arguments.')
end

Ntau = round(tau / Ts); % samples of one burst
NT = round(T / Ts); % samples between two consecutive bursts
N = NT * Nf; % total number of samples

n = (0 : N - 1)'; % indexes
t = n * Ts; % time array

V = zeros(size(n)); % signal samples

ni = (0 : Ntau - 1)'; % indexes of one burst

for i = 1:Nf
    f = fmin + (i - 1)*delta_f; % frequency of burst i
    F = f / fs; % digital frequency of burst i
    V(ni + NT * (i - 1) + 1) = sin(2 * pi * F * ni); % burst i
end

end